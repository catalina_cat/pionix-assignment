<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use GuzzleHttp;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

/**
 * Rest controller for conversions
 *
 * @package AppBundle\Controller
 * @author Catalina Cajvaneanu <catalina@mawas.ro>
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    private $API_KEY = "123456";
    /**
     * @Route("/convert", name="_convert")
     * @Method("GET")
     */
    public function getConversion(Request $request)
    {
        if ($request->getMethod() !== "GET") {
            return Response::create(json_encode(["reason" => "Not Permitted", "status" => "405"]), 405);
        }
        if(empty($request->query->get("api_key"))) {
            return Response::create(json_encode(["reason" => "API key not present", "status" => "401"]), 401);
        }
        if ($request->query->get("api_key") !== $this->API_KEY) {
            return Response::create(json_encode(["reason" => "Wrong credentials", "status" => "401"]), 401);
        }
        $response = array();
        $response['currency'] = "EUR";
        $base_url = "http://api.fixer.io/";
        $default_date = "latest";
        $query_arr = array();
        $amount = 1;
        if (!empty($request->query)) {
            if (!empty($request->query->get("date"))) {
                $base_url .= $request->query->get("date");
            } else
                $base_url .= $default_date;
            foreach ($request->query as $key => $value) {
                switch ($key) {
                    case "amount":
                        $amount *= floatval($value);
                        break;
                    case "currency":
                        $query_arr['base'] = $value;
                        break;
                    case "to":
                        $query_arr['symbols'] = $value;
                        $response['currency'] = $value;
                        break;
                    default:
                        break;
                }
            }
        }
        $client = new GuzzleHttp\Client();
        try {
            $query_response = $client->request('GET', $base_url, ['query' => $query_arr]);
            $currency_output = GuzzleHttp\json_decode($query_response->getBody()->getContents(), true);
            if (!empty($request->query->get("to")) && !empty($currency_output['rates'])) {
                $convert_output = floatval(current($currency_output['rates'])) * $amount;
                $response['value'] = $convert_output;
            }
            return Response::create(json_encode($response), $query_response->getStatusCode());
        } catch (RequestException $e) {
            preg_match('/"error\\":\\"([a-zA-Z ]*)/', $e->getMessage(), $m);
            return Response::create(json_encode(["reason" => $m[1], "status" => $e->getResponse()->getStatusCode()]), $e->getResponse()->getStatusCode());
        }
    }
}
